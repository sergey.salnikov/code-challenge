from django.contrib import admin
from wingtel.att_subscriptions.models import ATTSubscription


@admin.register(ATTSubscription)
class AdminATTSubscription(admin.ModelAdmin):
    list_display = ('user', 'id', 'status', 'deleted')
