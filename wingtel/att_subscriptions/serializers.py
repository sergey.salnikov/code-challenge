from rest_framework import serializers

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.sprint_subscriptions.mixins import SubscriptionActivationSerializerMixin


class ATTSubscriptionSerializer(serializers.ModelSerializer):
    """
    Default serializer for ATTSubscription model. "user" field is hidden and 
    defaults to current active user for associating subscriptions with it.
    "status" field is read-only and defaults to model's default value on creation
    """
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = ATTSubscription
        fields = ('plan', 'device_id', 'phone_number', 'phone_model',
                  'network_type', 'effective_date', 'user')
        read_only_fields = ('status',)


class ATTSubscriptionActivationSerializer(SubscriptionActivationSerializerMixin,
                                          serializers.ModelSerializer):
    """
    Subscription activation serializer for ATTSubscription model. In order
    to validate nullable/empty fields we can override serializer's fields using
    "extra_kwarg" attribute
    """
    class Meta:
        model = ATTSubscription
        fields = ('plan', 'device_id', 'phone_number', 'phone_model',
                  'network_type', 'effective_date')
        extra_kwargs = {
            'plan': {'allow_null': False},
            'device_id': {'allow_blank': False},
            'phone_number': {'allow_blank': False},
            'phone_model': {'allow_blank': False},
            'network_type': {'allow_blank': False},
            'effective_date': {'allow_null': False},
        }
