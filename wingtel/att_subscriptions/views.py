from rest_framework import permissions, viewsets
from rest_framework.response import Response

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.sprint_subscriptions.mixins import SubscriptionViewMixin
from wingtel.att_subscriptions.serializers import (
    ATTSubscriptionActivationSerializer, ATTSubscriptionSerializer)


class ATTSubscriptionViewSet(SubscriptionViewMixin, viewsets.ModelViewSet):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions.
    """
    queryset = ATTSubscription.objects.all()
    serializer_class = ATTSubscriptionSerializer
    subscription_model = ATTSubscription
    permission_classes = (permissions.IsAuthenticated,)
    activation_serializer = ATTSubscriptionActivationSerializer

    def get_queryset(self):
        """Shows user only related subscriptions"""
        return self.queryset.filter(user=self.request.user, deleted=False)
