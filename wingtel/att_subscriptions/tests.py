from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient

from wingtel.att_subscriptions.models import ATTSubscription
from wingtel.att_subscriptions.views import ATTSubscriptionViewSet
from wingtel.purchases.models import Purchase


class ATTSubscriptionViewTestCase(TestCase):
    fixtures = ['fixtures.json']
    base_url = '/api/att_subscriptions/'

    def setUp(self):
        self.user = User.objects.first()
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        self.subscriptions = ATTSubscription.objects.filter(user=self.user)

    def _get(self, url):
        """Helper method for activation api calls"""
        return self.client.get(url, format='json')

    def test_list_subscriptions(self):
        """User should get only a list of all his subscriptions"""
        response = self.client.get(self.base_url, format='json')
        self.assertEqual(len(response.data), 3)

    def test_activate_with_empty_fields(self):
        """User can not activate subsctiption with selected empty fields"""
        incorrect_sub = self.subscriptions.select_related('plan').get(status=ATTSubscription.STATUS.new)
        incorrect_sub.network_type = ''
        incorrect_sub.effective_date = None
        incorrect_sub.save()
        response = self._get(f'{self.base_url}{incorrect_sub.id}/activate/')
        self.assertDictEqual({
            'network_type': ['This field may not be blank.'],
            'effective_date': ['This field may not be null.']
        }, response.json())
        # Purcases shouldn't be created
        self.assertFalse(Purchase.objects.filter(user=self.user).count())

    def test_activate_correct_subscription(self):
        new_sub = self.subscriptions.select_related('plan').get(status=ATTSubscription.STATUS.new)
        response = self._get(f'{self.base_url}{new_sub.id}/activate/')
        self.assertTrue('message' in response.data)
        # Subscription should change it's status
        self.assertFalse(ATTSubscription.objects.filter(user=self.user, status=ATTSubscription.STATUS.new))
        # Purchase model should be created for current user
        purchases = Purchase.objects.filter(user=self.user)
        self.assertEqual(purchases.count(), 1)
        self.assertEqual(purchases.first().amount, new_sub.plan.price)

    def test_activate_wrong_subsctiption(self):
        """User can only activate new subscriptions"""
        active_sub = self.subscriptions.get(status=ATTSubscription.STATUS.active)
        response = self._get(f'{self.base_url}{active_sub.id}/activate/')
        self.assertDictEqual({
            'non_field_errors': ['Only subscriptions with status `New` can be activated'],
        }, response.data)
