from django.contrib import admin
from wingtel.purchases.models import Purchase


@admin.register(Purchase)
class AdminPurchase(admin.ModelAdmin):
    list_filter = ('user', 'status', 'created_at')
    list_display = ('user', 'status', 'amount', 'payment_date', 'created_at')
