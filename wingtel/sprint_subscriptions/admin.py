from django.contrib import admin
from wingtel.sprint_subscriptions.models import SprintSubscription


@admin.register(SprintSubscription)
class AdminSprintSub(admin.ModelAdmin):
    list_display = ('user', 'id', 'status', 'deleted')
