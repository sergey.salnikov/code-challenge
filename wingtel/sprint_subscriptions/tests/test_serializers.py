from django.contrib.auth.models import User
from django.test import TestCase

from rest_framework.serializers import ValidationError
from wingtel.purchases.models import Purchase
from wingtel.sprint_subscriptions.models import SprintSubscription
from wingtel.sprint_subscriptions.serializers import (
    SprintSubscriptionActivationSerializer, SprintSubscriptionSerializer)


class SprintSubscriptionViewTestCase(TestCase):
    fixtures = ['fixtures.json']

    def setUp(self):
        self.subscription = SprintSubscription.objects.get(pk=2)
        self.model_data = SprintSubscriptionSerializer(instance=self.subscription).data

    def test_correct_validatation(self):
        serializer = SprintSubscriptionActivationSerializer(instance=self.subscription, data=self.model_data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        self.assertEqual(Purchase.objects.filter(user=self.subscription.user).count(), 1)

    def test_expired_subscription(self):
        self.subscription.status = SprintSubscription.STATUS.suspended
        serializer = SprintSubscriptionActivationSerializer(instance=self.subscription, data=self.model_data)
        self.assertFalse(serializer.is_valid())
        self.assertDictEqual({
            'non_field_errors': ['Only subscriptions with status `New` can be activated']
        }, serializer.errors)

    def test_empty_fields_validation(self):
        self.subscription.device_id = ''
        self.subscription.effective_date = None
        self.model_data = SprintSubscriptionSerializer(instance=self.subscription).data
        serializer = SprintSubscriptionActivationSerializer(instance=self.subscription, data=self.model_data)
        self.assertFalse(serializer.is_valid())
        self.assertDictEqual({
            'device_id': ['This field may not be blank.'],
            'effective_date': ['This field may not be null.']
        }, serializer.errors)
