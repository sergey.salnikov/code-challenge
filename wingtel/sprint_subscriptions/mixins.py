from django.utils.timezone import now
from rest_framework import serializers
from rest_framework.decorators import action
from rest_framework.response import Response

from wingtel.purchases.models import Purchase
from wingtel.sprint_subscriptions.permissions import IsObjectOwner


class SubscriptionViewMixin:
    """
    Adds 'activate' detail endpoint for viewset

    In order to use this mixin "activation_serializer" variable must be provided.
    Activation serializer should validate object before activation 
    and update our instance. 
    """
    activation_serializer = None

    def __init__(self, *args, **kwargs):
        assert self.activation_serializer, \
            '`activation_serializer` must be set in order to use this endpoint'
        return super().__init__(*args, **kwargs)

    @action(detail=True, methods=['GET'], permission_classes=[IsObjectOwner])
    def activate(self, request, pk=None):
        """
        Endpoint for getting subscription instance, deserializing it and
        serizalizing it using "activation_serializer" for custom validation and
        custom saving logic.
        """
        instance = self.get_object()  # The only way "IsObjectOwner" been triggered
        instance_data = self.serializer_class(instance=instance).data
        serializer = self.activation_serializer(
            instance=instance,
            data=instance_data,
            context={'request': self.request}
        )
        if serializer.is_valid():
            serializer.save()
            return Response({'message': 'Subscription activated'}, status=200)
        return Response(serializer.errors, status=400)


class SubscriptionActivationSerializerMixin:
    """
    Base serializer for activating subscription models.

    This serializer validates subscription instances and will raise ValidationError
    if subscription status not being "new". On successfull validation, instance
    status will be changed to "active" and Purchase object will be created with
    "overdue" status.
    """

    def validate(self, attrs):
        if self.instance.status != self.instance.STATUS.new:
            raise serializers.ValidationError(
                'Only subscriptions with status `New` can be activated')
        return attrs

    def update(self, instance, validated_data):
        """Changes instance status to "active" and creates new Purchase"""
        instance.status = instance.STATUS.active
        Purchase.objects.create(
            user=instance.user,
            status=Purchase.STATUS.overdue,
            amount=instance.plan.price,
            payment_date=now()
        )
        instance.save(update_fields=['status'])
        return instance
