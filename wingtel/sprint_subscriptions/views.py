from rest_framework import permissions, viewsets

from wingtel.sprint_subscriptions.mixins import SubscriptionViewMixin
from wingtel.sprint_subscriptions.models import SprintSubscription
from wingtel.sprint_subscriptions.serializers import (
    SprintSubscriptionActivationSerializer, SprintSubscriptionSerializer)


class SprintSubscriptionViewSet(SubscriptionViewMixin, viewsets.ModelViewSet):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions.
    """
    queryset = SprintSubscription.objects.all()
    serializer_class = SprintSubscriptionSerializer
    activation_serializer = SprintSubscriptionActivationSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        """Shows user only related subscriptions"""
        return self.queryset.filter(user=self.request.user, deleted=False)
