from rest_framework import serializers

from wingtel.purchases.models import Purchase
from wingtel.sprint_subscriptions.models import SprintSubscription
from wingtel.sprint_subscriptions.mixins import SubscriptionActivationSerializerMixin


class SprintSubscriptionSerializer(serializers.ModelSerializer):
    """
    Default serializer for SprintSubscription model. "user" field is hidden and 
    defaults to current active user for associating subscriptions with it.
    "status" field is read-only and defaults to model's default value on creation
    """
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = SprintSubscription
        fields = ('id', 'plan', 'device_id', 'phone_number', 'phone_model',
                  'sprint_id', 'effective_date', 'user', 'status')
        read_only_fields = ('status',)


class SprintSubscriptionActivationSerializer(SubscriptionActivationSerializerMixin,
                                             serializers.ModelSerializer):
    """
    Subscription activation serializer for SprintSubscription model. In order
    to validate nullable/empty fields we can override serializer's fields using
    "extra_kwarg" attribute
    """
    class Meta:
        model = SprintSubscription
        fields = ('plan', 'device_id', 'phone_number', 'phone_model',
                  'sprint_id', 'effective_date')
        extra_kwargs = {
            'plan': {'allow_null': False},
            'device_id': {'allow_blank': False},
            'phone_number': {'allow_blank': False},
            'phone_model': {'allow_blank': False},
            'sprint_id': {'allow_null': False},
            'effective_date': {'allow_null': False},
        }
